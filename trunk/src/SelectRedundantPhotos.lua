local LrFunctionContext = import'LrFunctionContext'
local iterateFolders = require('PhotoUtil').iterateFolders

-- async (required by some functions in the method body)
-- withContext (needed for error handling)
LrFunctionContext.postAsyncTaskWithContext("LrQuickTools", function(context)
    iterateFolders(context, {
        statusText = "$$$/ch/wehrle/lightroom/Message/SelectStatus=Selected ^1 redundant photos.",
        selectPhotos = true,
    })
end)
