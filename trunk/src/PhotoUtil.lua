-- global
local LrLogger = import'LrLogger'
local LrApplication = import'LrApplication'
local LrPathUtils = import'LrPathUtils'
local LrDialogs = import'LrDialogs'
local LrTasks = import'LrTasks'

--local
local PrefsUtil = require 'PrefsUtil'

-- init global stuff
local logger = LrLogger('redundantLogger')
logger:enable('print')
local catalog = LrApplication.activeCatalog()


local function mustBeMarkedAndHasRawSibling(photo, catalog, fileFormatsToBeMarked)
    local currentPhotoFileFormat = photo:getRawMetadata("fileFormat")

    if fileFormatsToBeMarked[currentPhotoFileFormat] ~= nil then
        local rawSiblings = catalog:findPhotos{
            searchDesc = {
                {
                    criteria = "filename",
                    operation = "beginsWith",
                    value = LrPathUtils.removeExtension(photo:getFormattedMetadata("fileName")),
                    value2 = "",
                },
                {
                    {
                        criteria = "fileFormat",
                        operation = "==",
                        value = "DNG",
                    },
                    {
                        criteria = "fileFormat",
                        operation = "==",
                        value = "RAW",
                    },
                    combine = "union",
                },
                {
                    criteria = "folder",
                    operation = "any",
                    value = photo:getFormattedMetadata("folderName"),
                    value2 = "",
                },
                combine = "intersect",
            }
        }
        for _, sibling in ipairs(rawSiblings) do
            if LrPathUtils.removeExtension(sibling:getRawMetadata("path"))
                    == LrPathUtils.removeExtension(photo:getRawMetadata("path")) then
                return true
            end
        end
    end
    return false
end

return {
    iterateFolders = function(context, parameterBag)
        LrDialogs.attachErrorDialogToFunctionContext(context)

        -- modal progress window
        local progressScope = LrDialogs.showModalProgressDialog({
            title = LOC"$$$/ch/wehrle/lightroom/Message/Progress=Finding redundant photos",
            functionContext = context
        })

        local folders = {}
        local rejectedPhotos = {}
        local fileFormatsToBeMarked = PrefsUtil.getFileFormatsToBeMarked()

        for _, source in ipairs(catalog:getActiveSources()) do
            if type(source) ~= 'string' and 'LrFolder' == source:type() then
                table.insert(folders, source)
                logger:trace('Selected folder' .. source:getPath())
            end
        end

        for folderIndex, folder in ipairs(folders) do
            --time to repaint the progress bar
            LrTasks.sleep(0.03)
            if progressScope:isCanceled() then
                break
            end

            local allPhotosInFolder = folder:getPhotos(true)
            for photoIndex, photo in ipairs(allPhotosInFolder) do
                if progressScope:isCanceled() then
                    break
                end
                if mustBeMarkedAndHasRawSibling(photo, catalog, fileFormatsToBeMarked) then
                    table.insert(rejectedPhotos, photo)
                    logger:trace('Redundant photo' .. photo:getRawMetadata('path'))
                    if (parameterBag.modifyFunc) then
                        parameterBag.modifyFunc(photo, catalog)
                    end
                end
                progressScope:setPortionComplete(1 / #folders * (folderIndex - 1) + photoIndex / #allPhotosInFolder / #folders)
            end
        end

        if not progressScope:isCanceled() then
            progressScope:done()

            if #rejectedPhotos > 0 then
                if (parameterBag.selectPhotos) then
                    catalog:setSelectedPhotos(rejectedPhotos[1], rejectedPhotos)
                end
                if (parameterBag.statusText) then
                    LrDialogs.message(LOC(parameterBag.statusText, #rejectedPhotos), nil, "info")
                end
            else
                LrDialogs.message(LOC("$$$/ch/wehrle/lightroom/Message/ErrorStatus=No redundant photo found"), nil, "info")
            end
        end
    end,
}

