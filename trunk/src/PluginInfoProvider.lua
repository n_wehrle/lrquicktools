-- global
local LrView = import"LrView"
local LrPrefs = import"LrPrefs"

-- local
local PrefsUtil = require'PrefsUtil'

local function sectionsForTopOfDialog(f, _)
    local props = LrPrefs.prefsForPlugin()

    props.mustMarkJpeg = PrefsUtil.mustMarkJpeg()
    props.mustMarkTiff = PrefsUtil.mustMarkTiff()
    props.mustMarkPsd = PrefsUtil.mustMarkPsd()

    return {
        {
            title = LOC"$$$/ch/wehrle/lightroom/config/header=Configuration",
            bind_to_object = props,
            place = "horizontal",
            spacing = 20,

            f:column{
                spacing = f:control_spacing(),
                fill_horizontal = 1,
                f:row{
                    f:static_text{
                        title = LOC "$$$/ch/wehrle/lightroom/config/description=Please choose at least one file format:",
                    },
                },
                f:row{
                    f:checkbox{
                        title = LOC"$$$/ch/wehrle/lightroom/config/option/jpg=Mark JPEG files",
                        value = LrView.bind("mustMarkJpeg"),
                    },
                },
                f:row{
                    f:checkbox{
                        title = LOC"$$$/ch/wehrle/lightroom/config/option/tiff=Mark TIFF files",
                        value = LrView.bind("mustMarkTiff"),
                    },
                },
                f:row{
                    f:checkbox{
                        title = LOC"$$$/ch/wehrle/lightroom/config/option/psd=Mark PSD files",
                        value = LrView.bind("mustMarkPsd"),
                    },
                },
            },
       },
    }
end


return {
    sectionsForTopOfDialog = sectionsForTopOfDialog,
}