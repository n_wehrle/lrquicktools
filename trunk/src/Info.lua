--[[
  Useful tools for real world problems
  Feature 1: Flag redundant fotos

  Plugin:  LrQuickTools
  Version: 20140427(GIT af333be)
  Author:  Norman Wehrle
]]
return {
    LrSdkVersion = 5.0,
    LrSdkMinimumVersion = 4.0,
    LrToolkitIdentifier = 'ch.wehrle.lightroom.lrquicktools',
    LrPluginName = LOC"$$$/ch/wehrle/lightroom/PluginName=LrQuickTools",
    LrPluginInfoUrl = "https://bitbucket.org/n_wehrle/lrquicktools",

    LrPluginInfoProvider = "PluginInfoProvider.lua",

    -- LrInitPlugin = "Init.lua",
    -- LrPluginInfoProvider = "PluginInfoProvider.lua",

    -- only in file menu until yet
    LrExportMenuItems = {
        {
            title = LOC"$$$/ch/wehrle/lightroom/Menu/SelectRedundantPhotos=SelectRedundant",
            file = "SelectRedundantPhotos.lua",
        },
        {
            title = LOC"$$$/ch/wehrle/lightroom/Menu/RejectRedundantPhotos=RejectRedundant",
            file = "RejectRedundantPhotos.lua",
        },
    },

    VERSION = { display = "20140427(GIT af333be)" },
    LrAlsoUseBuiltInTranslations = true,
}
