local LrPrefs = import'LrPrefs'

-- defaults on first load
local markJpegOnFirstStart = true
local markTiffOnFirstStart = false
local markPsdOnFirstStart = false

local PrefsUtil = {}

function PrefsUtil.mustMarkJpeg()
    if (nil == LrPrefs.prefsForPlugin().mustMarkJpeg) then
        return markJpegOnFirstStart
    end
    return LrPrefs.prefsForPlugin().mustMarkJpeg
end

function PrefsUtil.mustMarkTiff()
    if (nil == LrPrefs.prefsForPlugin().mustMarkTiff) then
        return markTiffOnFirstStart
    end
    return LrPrefs.prefsForPlugin().mustMarkTiff
end

function PrefsUtil.mustMarkPsd()
    if (nil == LrPrefs.prefsForPlugin().mustMarkPsd) then
        return markPsdOnFirstStart
    end
    return LrPrefs.prefsForPlugin().mustMarkPsd
end

function PrefsUtil.getFileFormatsToBeMarked()
    local formats = {}
    if PrefsUtil.mustMarkJpeg() then
        formats["JPG"] = "JPG"
    end
    if PrefsUtil.mustMarkTiff() then
        formats["TIFF"] = "TIFF"
    end
    if PrefsUtil.mustMarkPsd() then
        formats["PSD"] = "PSD"
    end
    return formats
end

return PrefsUtil