-- Plugin
"$$$/ch/wehrle/lightroom/PluginName=LrQuickTools"

-- Configuration
"$$$/ch/wehrle/lightroom/config/header=Configuration"
"$$$/ch/wehrle/lightroom/config/description=Please choose at least one file format:"
"$$$/ch/wehrle/lightroom/config/option/jpg=Mark JPEG files"
"$$$/ch/wehrle/lightroom/config/option/tiff=Mark TIFF files"
"$$$/ch/wehrle/lightroom/config/option/psd=Mark PSD files"

-- Menu Entries
"$$$/ch/wehrle/lightroom/Menu/SelectRedundantPhotos=Select Redundant Photos"
"$$$/ch/wehrle/lightroom/Menu/RejectRedundantPhotos=Reject Redundant Photos"

-- Windows
-- Messages
"$$$/ch/wehrle/lightroom/Message/SelectStatus=Selected ^1 redundant photos."
"$$$/ch/wehrle/lightroom/Message/RejectStatus=Rejected ^1 redundant photos."
"$$$/ch/wehrle/lightroom/Message/ErrorStatus=No redundant photos found. That might be OK.^n^nMaybe you forgot to choose a file format in the plugin configuration dialog? Maybe you forgot to choose a folder in the left Lightroom panel? The plugin does not work with collections yet."
"$$$/ch/wehrle/lightroom/Message/Progress=Finding redundant photos"

