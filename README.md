# LrQuickTools
## Find redundant photos

This Lightroom plugin is able to find and select photos that have a RAW sibling. The plugin is useful if you shoot one day RAW-only, the other day RAW/JPEG and the next day only JPEG. If you want to clean up your library, you will only delete JPEG's that have a RAW counterpart and won't loose any data.

![coverpic](https://bitbucket.org/n_wehrle/lrquicktools/raw/master/coverpic.jpg)