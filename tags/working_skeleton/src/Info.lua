--[[
  Useful tools for real world problems

  Plugin:  LrQuickTools
  Version: 20101118(SVN 176M)
  Author:  Norman Wehrle
]]
return {
	
	LrSdkVersion = 3.0,
	LrSdkMinimumVersion = 3.0,

    LrToolkitIdentifier = 'ch.wehrle.lightroom.lrquicktools',
    LrPluginName = LOC "$$$/lidiplu/PluginName=LrQuickTools",
    LrPluginInfoUrl = "http://sourceforge.net/projects/lrquicktools/",
	
    -- LrPluginInfoProvider = "PluginInfoProvider.lua",

	-- only in file menu until yet
    LrExportMenuItems = {
      {
		title = LOC "$$$/lidiplu/Menu/FlagRedundantPhotos=Flag Redundant Photos",
		file = "FlagRedundantPhotos.lua",
      },
	},

    --[[
    LrHelpMenuItems = {
		title = LOC "$$$/lidiplu/Menu/DevSettingsCompareHelp=DevSettingsCompare Help",
		file = "Help.lua",
	},
	--]]

	VERSION = { display="20101118(SVN 176M)" },
    LrAlsoUseBuiltInTranslations = true,
}


	