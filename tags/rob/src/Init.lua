-- L R   M O D U L E S

-- Baseline:
_G.LrPathUtils = import'LrPathUtils'
_G.LrErrors = import'LrErrors'
_G.LrApplication = import'LrApplication'
_G.LrTasks = import'LrTasks'
_G.LrShell = import'LrShell'
_G.LrStringUtils = import'LrStringUtils'
_G.LrDate = import'LrDate'
_G.LrProgressScope = import'LrProgressScope'
_G.LrFunctionContext = import'LrFunctionContext'
_G.LrPrefs = import'LrPrefs'
_G.LrView = import'LrView'
_G.LrBinding = import'LrBinding'
_G.LrDialogs = import'LrDialogs'
_G.LrLogger = import'LrLogger'
_G.LrFileUtils = import'LrFileUtils'
_G.LrRecursionGuard = import'LrRecursionGuard'
_G.LrSystemInfo = import'LrSystemInfo'
_G.LrHttp = import'LrHttp'

_G.lr = {}

local frameworkDir = LrPathUtils.child(_PLUGIN.path, 'Framework')
local loadDir = LrPathUtils.child(frameworkDir, "System") -- loader is in system library.
local loadFile = LrPathUtils.child(loadDir, "Load.lua")
local status, loader = pcall(dofile, loadFile)
if status then
  _G.Load = loader.init(frameworkDir)
else
  LrErrors.throwUserError(loader or 'nil')
end


_G.LrExportSession = import'LrExportSession'
_G.LrExportContext = import'LrExportContext'

-- Publish option
_G.LrPublishService = import'LrPublishService'
_G.LrPublishedCollection = import'LrPublishedCollection'
_G.LrPublishedCollectionSet = import'LrPublishedCollectionSet'
_G.LrPublishedPhoto = import'LrPublishedPhoto'


-- Catalog Organization option
_G.LrCollection = import'LrCollection'
_G.LrCollectionSet = import'LrCollectionSet'
_G.LrFolder = import'LrFolder'
_G.LrKeyword = import'LrKeyword'

-- Photo option
_G.LrPhoto = import'LrPhoto'
_G.LrPhotoInfo = import'LrPhotoInfo'


-- FTP option
_G.LrFtp = import'LrFtp'

-- HTTP option
_G.LrHttp = import'LrHttp'

-- Localization option
_G.LrLocalization = import'LrLocalization'

-- Develop option
_G.LrDevelopPreset = import'LrDevelopPreset'
_G.LrDevelopPresetFolder = import'LrDevelopPresetFolder'

-- XML option
_G.LrXml = import'LrXml'


-- These are not yet used by nor supported in the base classes,
-- as such, they must be loaded explicitly by app if needed.
_G.LrMD5 = import'LrMD5'
_G.LrMath = import'LrMath'
_G.LrPasswords = import'LrPasswords'
_G.LrColor = import'LrColor'


-- Namespace not needed:
-- lr-catalog
-- lr-observable-table
-- lr-plugin
-- lr-web-view-factory




-- Common Globals
_G.lr.prefs = LrPrefs.prefsForPlugin() -- *** Use sparingly: go through App/Prefs API whenever possible.
_G.lr.catalog = LrApplication.activeCatalog()
_G.lr.vf = LrView.osFactory()

lr.prefs.testMode = lr.prefs.testMode or false -- optional but satisfying.


-- Framework modules as globals.
_G.Object = Load.frameworkModule('System/Object') -- required.
_G.String = Load.frameworkModule('Data/String') -- required.
_G.Boolean = Load.frameworkModule('Data/Boolean') -- not sure, but its tiny.
_G.Number = Load.frameworkModule('Data/Number') -- ditto
_G.DateTime = Load.frameworkModule('Data/DateTime') -- probably optional, but also: small.
_G.Table = Load.frameworkModule('Data/Table') -- required.
-- _G.Xml = Load.frameworkModule( 'Data/Xml' ) -- optional.
_G.Disk = Load.frameworkModule('FileSystem/Disk') -- required.
-- _G.Catalog = Load.frameworkModule( 'Catalog/Catalog' ) -- optional, maybe omitted if export plugin, but dang near everything else could use it.
_G.Dialog = Load.frameworkModule('Gui/Dialog') -- required.
-- _G.View = Load.frameworkModule( 'Gui/View' ) -- optional, I think.
_G.App = Load.frameworkModule('System/App') -- required.
_G.OperatingSystem = Load.frameworkModule('System/OperatingSystem') -- Required.
if WIN_ENV then
  _G.Windows = Load.frameworkModule('System/Windows')
else
  _G.Mac = Load.frameworkModule('System/Mac')
end
_G.Properties = Load.frameworkModule('System/Properties') -- required.
-- _G.Preferences = Load.frameworkModule( 'System/Preferences' ) -- optional, but highly recommended if you will support preferences in plugin manager.
_G.LogFile = Load.frameworkModule('System/LogFile') -- required.
_G.Call = Load.frameworkModule('System/Call') -- required.
_G.Service = Load.frameworkModule('System/Service') -- optional - needed for exports or substantial menu initiated services...
_G.User = Load.frameworkModule('System/User') -- required.
_G.App = Load.frameworkModule('System/App') -- required.
_G.Manager = Load.frameworkModule('Gui/Manager') -- required.
-- _G.SpecialManager = Load.anyModule( 'SpecialManager' ) -- presently required, although could easily be recoded to be optonal.
-- default implementation is tiny anyway.
_G.ObjectFactory = Load.frameworkModule('System/ObjectFactory') -- required.
-- _G.SpecialObjectFactory = Load.anyModule( 'SpecialObjectFactory' ) -- presently required, although could easily be recoded to be optonal.

if lr.prefs.advDbgEna then
  _G.AdvancedDebug = Load.frameworkModule('System/AdvancedDebug')
end

_G.LrExportSession = import'LrExportSession'
_G.LrExportSettings = import'LrExportSettings'
_G.LrMD5 = import'LrMD5'
_G.Export = Load.frameworkModule('ExportAndPublish/Export')
_G.Preferences = Load.frameworkModule('System/Preferences')
-- _G.Common = Load.anyModule( 'Common' )
-- _G.SpecialExport = Load.anyModule( 'SpecialExport' )

_G.objectFactory = ObjectFactory:new() -- use default class factory. Used to create objects owned by other objects, i.e. non-globals.
_G.props = Properties:new()
_G.fso = Disk:new() -- file-system-object.
_G.app = App:new()
_G.dialog = Dialog:new()
if View then
  _G.view = View:new()
end
if Catalog then
  _G.catalog = Catalog:new() -- be sure to use lr.catalog if that's what you mean.
end
_G.bool = Boolean:new()
_G.date = DateTime:new() -- Time => implied.
_G.num = Number:new() -- Time => implied.
_G.str = String:new()
_G.tab = Table:new()
if Xml then
  _G.xml = Xml:new()
end
-- _G.manager = SpecialManager:new()
_G.shutdown = false
